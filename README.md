Bottokens:

* Gilles: Mjc2NjgyMTI0MjIyNzkxNjgw.C3SwGw.PHKnp2OAzgFIyA7JyAVWwbS9xd0
* Paladins: MzAzNDQ1OTIzMjY5MzEyNTEy.C9YN0w.RyTG7q8jJjATgB6cxtJxsd3e0Ts

Invite Link für Babiel Bot:
https://discordapp.com/api/oauth2/authorize?client_id=303445923269312512&scope=bot&permissions=0

Globale Variablen:

* process => Aktueller Prozess (Standard NodeJS)
* cwd => Root Ordner
* _ => Underscore
* _hb => Handlebars
* _cfg => config.json (im Root Ordner)
* _client => Discord Client Object (Bot User)
* _fm => FolderManager (Gibt die Pfade zu den wichtigstens Applikationsordnern zurück)
* _uh => UserHandler Object
* _commandHandler => Command Handler Instanz

FAQ:
* Ich will in einem Command einen anderen Command aufrufen:_commandHandler.executeCmd("andererCommandName",msg)
* Ich habe als Admin den CMD-Prefix kaputt gemacht: ###resetPrefix  <-- Befehl zum resetten
* Wie lautet der aktuelle CMD-Prefix: ###showPrefix
* Ich will Ordner/Dateien ab der index.js (Rootfile) einlesen/schreiben: cwd enthält den Pfad bis zur index.js


