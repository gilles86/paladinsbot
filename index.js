/**
 * Created by Gilles on 02.02.2017.
 */

var connected = false;
var connectInterval = null;


global.cwd = __dirname;
const FolderManager = require(cwd+"/src/FolderManager.js");
const System = require(cwd+"/src/System.js");
const UserHandler = require(cwd+"/src/UserHandler.js");
const Discord = require("discord.js");
global.Discord = Discord;
const CommandHandler = require(cwd+"/src/CommandHandler.js");
const ServiceCommands = require(cwd+"/src/ServiceCommands.js");
const fs = require("fs");
const HirezAPIManager = require(cwd+"/src/HirezAPIManager");



global.events = new (require(cwd+"/src/EventManager.js"))();

global.logger = require("winston");
logger.add(logger.transports.File, { filename: cwd+'/log/debug.log' });
logger.level = "debug";

const hb = require("handlebars");
global._hb = hb;
require(cwd+"/src/HandlebarsHelper.js")();
global._ = require("underscore");
var loadedServices = false;

global._system = new System();
global._cfg = _system.cfg;
global.paladins = new HirezAPIManager(_cfg.devId, _cfg.authKey);
global._fm = new FolderManager(_cfg);
global._helper = require(cwd+"/src/HelperFunctions.js");
global._uh = new UserHandler();


var cmdPrefixes = {};
try {
  cmdPrefixes = JSON.parse(fs.readFileSync(cwd+'/cmdPrefixes.json', 'utf8'));
} catch(error) {
  console.warn("The File cmdPrefixes.json does not exists");
}

const client = new Discord.Client();
global._client = client;
const commandHandler = new CommandHandler(_cfg, cmdPrefixes);
global._commandHandler = commandHandler;

global._serviceCommands = new ServiceCommands(commandHandler, _cfg);

global.COMMAND = {
  "WRONG_FORMAT" : Symbol("wrong_command_format"),
  "NO_COMMAND" : Symbol("no_command"),
  "NOT_FOUND" : Symbol("command_not_found")
};

client.on('ready', () => {
  if(!loadedServices) {
    loadedServices = true;
    _serviceCommands.start();
  }
  if(connectInterval) {
    clearInterval(connected);
  }
  connected = true;
  logger.debug(`Logged in as ${client.user.username}!`);
  console.log(`Logged in as ${client.user.username}!`);
});

client.on("raw", function(raw) {
  logger.debug(raw);
});

/**
 * @param {Message} msg
 */
client.on('message', msg => {

  var date = new Date();
  date = date.toLocaleDateString()+" "+date.toLocaleTimeString();
  console.log(date+": received: "+msg.author.username+": "+msg.content);

  if(msg.content == "###resetPrefix") {
    _system.setCmdPrefix("-");
    _system.writeConfig();
    msg.reply("My new Prefix is -");
  }
  if(msg.content ==  "###showPrefix") {
    msg.reply("My new Prefix is "+_cfg.cmdprefix);
  }

  events.trigger("message", {
    "msg" : msg
  });

  if(commandHandler.isCommand(msg) || commandHandler.isSpecialCommand(msg)) {
    var response = commandHandler.execute(msg);
    if(response == COMMAND.NOT_FOUND) {
      var cmdName = commandHandler.getCommandName(msg);
      msg.channel.sendMessage("Command "+cmdName+" wasn't found");
    }
  }
});

client.on("disconnect", function() {
  connected = false;
  connectInterval = setInterval(function() {
    if(connected) {
      clearInterval(connected);
    } else {
      //client.login(_cfg.bottoken);
      logger.log("disconnected!!");
    }
  }, 5000);
});

client.login(_cfg.bottoken);