let extend = require("extend");

module.exports= class EventManager {
  
  constructor() {
    this._eventlist = {};
  }
  
  
  on(eventName,callback, staticData) {
    this._createEventSpace(eventName);
    this._addEvent(this._createEvtObject(eventName,callback, staticData));
  }
  
  off(eventName, callback) {
    if(!eventName)return null;
    
    if(!callback) {
      this._simpleOff(eventName);
    } else {
      this._advancedOff(eventName, callback);
    }
  }
  
  _simpleOff(eventName) {
    if(this._eventlist[eventName]) {
      this._eventlist[eventName] = [];
    }
  }
  
  _advancedOff(eventName, cb) {
    var hash = this._getFunctionHash(cb);
    var evtlist = this._eventlist[eventName];
    for(var i=0; i < evtlist.length; i++) {
      var currentEvt = evtlist[i];
      if(hash == currentEvt.hash) {
        evtlist.splice(i,1);
        i--;
      }
    }
    this._eventlist[eventName] = evtlist;
  }
  
  trigger(eventname, data) {
    this._createEventSpace(eventname);
    var space = this._eventlist[eventname];
    var length = space.length;
    
    for(var i=0; i < length; i++) {
      var evt = space[i];
      var returnvalue = evt.cb(extend(true, {}, evt.staticData||{}, data||{}));
    }
  }
  
  _addEvent(evt) {
    this._eventlist[evt.eventname].push(evt);
  }
  
  _createEventSpace(eventName) {
    if(!this._eventlist[eventName]) {
      this._eventlist[eventName] = [];
    }
  }
  
  
  
  
  
  _createEvtObject(evtName, callback, staticData) {
    var cbHash = this._getFunctionHash(callback);
    return {
      "cb" : callback,
      "hash" : cbHash,
      "eventname" : evtName,
      "staticData" : staticData
    };
  }
  
  
  _getFunctionHash(fn) {
    return this._hashCode(fn.toString().trim().replace(/[\r\n]*/g,""));
  }
  
  
  
  _hashCode(text) {
    var hash = 0, i, chr, len;
    if (text.length === 0) return hash;
    for (i = 0, len = text.length; i < len; i++) {
      chr   = text.charCodeAt(i);
      hash  = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return ""+hash;
  }
  
};