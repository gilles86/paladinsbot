/**
 * Created by Gilless on 02.02.2017.
 */
module.exports = function() {
  _hb.registerHelper('ifCond', function(v1, v2, options) {
    if(v1 === v2) {
      return options.fn(this);
    }
    return options.inverse(this);
  });
  
  _hb.registerHelper("inc", function(value, options)
  {
    var val = parseInt(value) + 1;
    if(val < 10) {
      val = "0"+val;
    }
    return val;
  });
  
  _hb.registerHelper("onlysmallletters", function(value, options)
  {
    return value.replace(/'/g,"").replace(/ /g, "").toLowerCase();
  });
  
  _hb.registerHelper("addition", function(value1, value2, options)
  {
    return parseInt(value1) + parseInt(value2);
  });
  
  _hb.registerHelper("leadingzero", function(value1, options)
  {
    return parseInt(value1) < 10 ? "0"+value1 : value1;
  });
  
  _hb.registerHelper("secondsToTime", function(value1, options)
  {
    var timeInt = parseInt(value1);
  });
  
  _hb.registerHelper("removefirstxchars", function(value1, value2, options)
  {
    return value1.substr(parseInt(value2));
  });
  
  

  _hb.registerHelper("xif", function (expression, options) {
    return _hb.helpers["x"].apply(this, [expression, options]) ? options.fn(this) : options.inverse(this);
  });

  _hb.registerHelper("x", function(expression, options) {
    var result;

    // you can change the context, or merge it with options.data, options.hash
    var context = this;

    // yup, i use 'with' here to expose the context's properties as block variables
    // you don't need to do {{x 'this.age + 2'}}
    // but you can also do {{x 'age + 2'}}
    // HOWEVER including an UNINITIALIZED var in a expression will return undefined as the result.
    with(context) {
      result = (function() {
        try {
          return eval(expression);
        } catch (e) {
          console.warn('•Expression: {{x \'' + expression + '\'}}\n•JS-Error: ', e, '\n•Context: ', context);
        }
      }).call(context); // to make eval's lexical this=context
    }
    return result;
  });
};