/**
 * Created by Gilles on 03.02.2017.
 */
const urlencode = require("urlencode");
const request = require("request");

module.exports = class Bot_Gif {

  /**
   * @param {Message} msg
   */
  execute(msg, args) {

    let topic = "random";
    if(args) {
      topic = urlencode(args);
    }

    this.getGifUrl(topic, function(url) {
      var img = new Discord.RichEmbed();
      img.setImage(url);
      msg.channel.sendEmbed(img);
    });
  }

  getGifUrl(topic, cb) {
    request("http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC&tag="+topic,function(err, data, body) {
      var dataObj = JSON.parse(body);
      cb(dataObj.data.image_url);
    });
  }

};