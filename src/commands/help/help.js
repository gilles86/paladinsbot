/**
 * Created by Gilles on 02.02.2017.
 */
const fs = require("fs");

module.exports = class Bot_Help {

  constructor() {
    this.commandList = null;
    this.privateCmds = this.readPrivateCmds();
  }

  /**
   * @param {Message} msg
   */
  execute(msg, args) {
    if(!args) {
      this.simpleHelp(msg);
    } else {
      this.helpForCmd(msg, args);
    }

  }

  getCommandList(cb) {
    var I = this;
    if(!this.commandList) {
      fs.readdir(cwd+"/src/"+_cfg.cmdfolder,"utf-8", function(err, files) {
        if(err) {
          console.log("Fehler beim lesen der Kommandos: "+err);
          cb("");
        } else {
          I.commandList = files;

          cb(I.commandList);
        }
      });
    } else {
      cb(I.commandList);
    }
  }

  simpleHelp(msg) {
    var I = this;
    this.getCommandList(function(files) {
      var cmds = files.slice();
      cmds = I.filterAdminCmdsIfNeeded(msg, cmds);
      _helper.renderHandleBars(msg,__dirname+"/message.hb", {"cmds" : cmds}, function(msg, text) {

      });
    });
  }

  filterAdminCmdsIfNeeded(msg, cmds) {
    if(!_uh.isAdminUser(msg.author)) {
      for(var i=0; i < this.privateCmds.length; i++) {
        var privName = this.privateCmds[i];
        var index = cmds.indexOf(privName);
        if(index >= 0) {
          cmds.splice(index,1);
        }

      }
    }
    return cmds;
  }

  helpForCmd(msg,args) {
    var I = this;
    this.getCommandList(function(files) {
      if(files.indexOf(args) > -1) {
        I.getHelpFileText(args, function(helptext) {
          if(helptext) {
            msg.reply(helptext);
          } else {
            msg.reply("Keine Hilfe für den Befehl "+args+" vorhanden");
          }

        });
      }
    });
  }

  getHelpFileText(args, cb) {
    fs.readFile(cwd+"/src/"+_cfg.cmdfolder+"/"+args+"/help.txt","utf-8", function(err, file) {
      if(err) {
        console.error("Helpfile für "+args+" konnte nicht ausgelesen werden: "+err);
        cb("");
      } else {
        cb(file);
      }

    });
  }

  readPrivateCmds() {
    return JSON.parse(fs.readFileSync(_fm.getDataFolder()+"/privateCmds.json"));
  }



};