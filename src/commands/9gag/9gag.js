/**
 * Created by Gilless on 03.02.2017.
 */
const gag = require("node-9gag")

module.exports = class Bot_9Gag {

  execute(msg, args) {
    const standardTopic = "trending";

    var topics = ["hot","fresh","trending"];
    var topic = (typeof args == "string") ? this.cleanTopic(args) : standardTopic;
    if(topics.indexOf(topic)  < 0) {
      topic = standardTopic;
    }

    gag.section(topic, function(err, res) {
      var posting = _.sample(res)
      msg.reply(posting.url);
    });
  }

  cleanTopic(topic) {
    topic = topic.split(" ")[0];
    topic = topic.trim();
    return topic;
  }

};