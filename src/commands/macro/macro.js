/**
 * Created by Georgi on 02.02.2017.
 */
const fs = require("fs");

module.exports = class Bot_Macro  {
  
  constructor() {
    this.macroList = [];
    this.macroFileLoaded = false;
  }

  /**
   * @param {Message} msg
   */
  execute(msg, args) {
    this.checkInput(msg, args);
  }

  checkInput( msg, args ) {
    var I = this;

    this.getMacroList(function(macros) {

      var input = args.split(" ");
      var macroName = input[0];
      var hasParameters = (input.length >=2 );

      if ( macroName === "all" || macroName === "list" ) { 
        var macrosString = "";
        if ( macros.length > 0 ) {
          for( var index in macros )  {
            macrosString += "\n* " + macros[index].name
          }
        } else {
          msg.channel.sendMessage("Es gibt noch keine Makros");
          return true;
        }
        
        msg.channel.sendMessage("Verfügbare Makros:\n"+macrosString);
        return true;
      }

      if(I.isSyntaxCorrect(input)) {
        
        switch ( input[0] ) {
          case "change":
          case "edit":
            I.editMacro ( input, msg, hasParameters ); 
            return true;
          break;
          case "delete":
          case "remove":
            I.deleteMacro ( input, msg );
            return true;
          break;
        }
        
        if ( hasParameters ) {
          var parameters = input.splice(1, input.length).join(" ");
          
          if( I.doesMacroExist( macroName ) ) {
            msg.channel.sendMessage("Makro " + macroName + " existiert bereits.")
          } else {

            if(parameters.trim() === "") {
              msg.channel.sendMessage("Dein Makro darf nicht leer sein");
            }

            var macro = {
              "name" : macroName,
              "binding" : parameters
            };
            
            I.addMacroToList(macro);

            msg.channel.sendMessage("Makro " + macroName + " wurde erstellt");
          }

        } else {
          if ( I.doesMacroExist( macroName ) ) { 
            I.playMacro( msg, macroName );
          } else {
            msg.channel.sendMessage("Makro " + macroName + " existiert nicht");
          }

        }

      } else {
        msg.channel.sendMessage("Syntax ist nicht korrekt.\nHilfe: " + _cfg.cmdprefix + "help macro");
      }
    });

  }

  isSyntaxCorrect ( input ) { 
    var pattern = new RegExp(/^[a-z]+$/);
    return ( input.length >= 1 && pattern.test(input[0]) );
  }

  doesMacroExist ( name )  {
    var macros = this.macroList;
    for(var i = 0 ; i < macros.length ; i ++ ) {
      var macro = macros[i];
      if(macro.name === name) {
        return true;
      }
    }
    return false;
  }

  getMacro (name ) {
    var macros = this.macroList;
    for(var i = 0 ; i < macros.length ; i ++ ) {
      var macro = macros[i];
      if(macro.name === name) {
        return macro;
      }
    }
  }

  playMacro ( msg, name ) {
    var macro = this.getMacro(name); 
    msg.channel.sendMessage( macro.binding );
  }

  saveMacros () {
    var macros = {
      macros : this.macroList
    };

    fs.writeFile( cwd +"/"+ _cfg.macro_cfgpath, JSON.stringify(macros), (err) => {
      if (err) {
        console.log(err);
      }
    });
  }

  addMacroToList ( macro ) { 
    if(!this.doesMacroExist (macro.name) ){
      this.macroList.push(macro);
      this.saveMacros();
    }
  }

  getMacroList(cb) {
    var I = this;
    if(!this.macroFileLoaded) {
      this.macroFileLoaded = true;
      fs.readFile(cwd +"/"+ _cfg.macro_cfgpath, "utf8", function(err, data) {
        if(err) {
          console.log("Fehler beim Lesen der Makrodatei");
          cb("");
        } else {
          I.macroList = JSON.parse(data).macros;
          cb(I.macroList);
        }
      });
    } else {
      cb(I.macroList);
    }
  }

  editMacro( input, msg, hasParameters ) {
    if( !hasParameters ) {
      msg.channel.sendMessage("Syntax ist nicht korrekt.\nHilfe: " + _cfg.cmdprefix + "help macro");
    } else {
      var macroName = input[1];
      if ( this.doesMacroExist(macroName) ) {
        var newBinding = input.splice(2, input.length).join(" ");
        if(newBinding.trim() === "") {
          msg.channel.sendMessage("Dein Makro darf nicht leer sein");
          return true;
        }
        var macros = this.macroList;
        for( var index in macros) {
          if(macros[index].name === macroName) {
            macros[index].binding = newBinding;
          }
        }
        this.saveMacros();
        msg.channel.sendMessage("Makro " + macroName + " wurde bearbeitet.");
      } else {
        msg.channel.sendMessage("Makro " + macroName + " existiert nicht.");
      }
    }
  }

  deleteMacro ( input, msg) {
    var macros = this.macroList;
    var unwantedMacro = input[1];
    if(!unwantedMacro) {
      msg.channel.sendMessage("Ich weiß nicht welches Makro ich entfernen soll...");
      return false;
    }
    if(this.doesMacroExist(unwantedMacro)) {
      var newMacroList = [];
      for(var index in macros ) {
        if( macros[index].name != unwantedMacro ) { 
          newMacroList.push ( macros[index] );
        }
      }

      this.macroList = newMacroList;
      this.saveMacros();
      msg.channel.sendMessage("Makro " + unwantedMacro + " wurde entfernt.");
    } else {
      msg.channel.sendMessage("Makro " + unwantedMacro + " existiert nicht.");
    }
  }

}