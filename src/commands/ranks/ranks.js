/**
 * Created by Gilles on 14.04.2017.
 */
class Bot_Ranks {
  execute(msg, args="") {
    let argList = args.split(" ");
    if(argList[0] == "") {
      msg.reply("Incorrect format. Please add a username");
      return false;
    }
    let name = argList[0];
    
    
    paladins.execute("ranks",name,(err,data) => {
      if(!err) {
        this.getText(data, function(text) {
          msg.reply(text);
        });
      } else {
        msg.reply("Error: "+err);
      }
    });
  }
  
  getText(data,cb) {
    _helper.renderHandleBars(null,__dirname+"/message.hb", {"champs" : data}, function(text) {
      cb(text);
    });
  }
}

module.exports = Bot_Ranks;