/**
 * Created by Gilles on 02.02.2017.
 */
module.exports = class Bot_Dogpic {

  /**
   * @param {Message} msg
   */
  execute(msg) {

    var date = new Date();
    var img = new Discord.RichEmbed();
    img.setImage("http://www.randomdoggiegenerator.com/randomdoggie.php?stamp"+date.getTime());
    msg.channel.sendEmbed(img);
  }

};