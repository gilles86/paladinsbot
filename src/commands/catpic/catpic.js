/**
 * Created by Gilles on 02.02.2017.
 */
module.exports = class Bot_Catpic {

  /**
   * @param {Message} msg
   */
  execute(msg) {

    var date = new Date();
    var img = new Discord.RichEmbed();
    img.setImage("http://thecatapi.com/api/images/get?format=src&type=gif&stamp"+date.getTime());
    msg.channel.sendEmbed(img);
  }

};