/**
 * Created by Gilles on 14.04.2017.
 */
class Bot_Player {
  execute(msg, args="") {
    let argList = args.split(" ");
    if(argList[0] == "") {
      msg.reply("Incorrect format. Please add a username");
      return false;
    }
    let name = argList[0];
    
    paladins.execute("player",name,(err,data) => {
      if(!err) {
        
       
        if(data.length > 0) {
          let playerData = data[0];
  
          let lastTimeInHours = _helper.durationInHours(playerData.Last_Login_Datetime);
          let lastTimeInDays = _helper.durationInDays(playerData.Last_Login_Datetime);
  
          if(lastTimeInHours <= 100) {
            playerData.Last_Login_Hours = lastTimeInHours
          } else {
            playerData.Last_Login_Days = lastTimeInDays;
          }
          this.getText(playerData,(text) => {
            msg.reply(text);
          });
        } else {
          msg.reply("There exists no Player with this name");
        }
      } else {
        msg.reply("Error: "+err);
      }
    });
  }
  
  
  getText(data,cb) {
    var I = this;
    _helper.renderHandleBars(null,__dirname+"/message.hb", data, function(text) {
      cb(text);
    });
  }
  
}

module.exports = Bot_Player;