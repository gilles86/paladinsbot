
Player: {{Name}}
Level: {{Level}}
Master-Level: {{MasteryLevel}}
Wins: {{Wins}}
Losses: {{Losses}}
Last Login: {{#if Last_Login_Hours}}{{Last_Login_Hours}} hours ago{{/if}}{{#if Last_Login_Days}}{{Last_Login_Days}} days ago{{/if}}