/**
 * Created by Gilless on 02.02.2017.
 */
let moment = require("moment");

module.exports = class Bot_ServerInfo {

  execute(msg, args) {

    if(!msg.guild) {
      msg.reply("Dieser Befehl funktioniert nicht im Privat Chat");
      return true;
    }

    let owner = msg.guild.owner.nickname;

    let date = moment(msg.guild.createdAt);
    msg.reply("Der Server wurde an diesem Datum erstellt: "+date.format("DD.MM.YYYY")+" und gehört aktuell "+owner);
  }

};