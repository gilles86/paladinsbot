/**
 * Created by Gilles on 05.02.2017.
 */
const fs = require("fs");
const shell = require("shelljs");
const getIP = require('external-ip')();

module.exports = class Bot_Admin {


  execute(msg, args) {
    if(_uh.isAdminUser(msg.author)) {
      this.doCommand(msg, args);
    } else {
      msg.reply("Dieser Befehl darf nur von einem Admin ausgeführt werden");
    }
  }

  doCommand(msg, args) {
    var argList = args.split(" ");
    var cmdName = argList.shift();
    if(!cmdName) {
      cmdName = "help";
    }

    var additionalArgs = argList;
    if(typeof this["cmd_"+cmdName] == "function") {
      this["cmd_"+cmdName](msg, additionalArgs);
    } else {
      msg.reply("Admin Befehl "+cmdName+" nicht gefunden");
    }
  }


  // CMD Section

  cmd_add_admin(msg, args) {
    var adminAddUser = _uh.getMentionedUser(msg.mentions);
    if(!adminAddUser) return false;
    if(!_uh.isAdminUser(adminAddUser)) {
      _uh.addAdminUser(adminAddUser);
      msg.reply("Benutzer "+adminAddUser.username+" zur Adminliste hinzugefuegt");
    } else {
      msg.reply("Der Benutzer "+adminAddUser.username+" ist schon Admin");
    }
  }

  cmd_remove_admin(msg,args) {
    var adminAddUser = _uh.getMentionedUser(msg.mentions);
    if(!adminAddUser) return false;
    var authorUser = msg.author;
    if(_uh.isAdminUser(adminAddUser)) {
      if(adminAddUser.id != authorUser.id) {
        _uh.removeAdminUser(adminAddUser);
        msg.reply("Benutzer "+adminAddUser.username+" von Adminliste entfernt");
      } else {
        msg.reply("Du kannst dich nicht selber von der Adminliste entfernen");
      }
    } else {
      msg.reply("Benutzer "+adminAddUser.username+" ist kein Admin");
    }

  }

  cmd_help(msg, args) {
    var allMethods = _helper.getAllClassMethods(this);
    var cmdMethods = [];
    for(var i=0; i < allMethods.length;i++) {
      if(allMethods[i].startsWith("cmd_")) {
        cmdMethods.push(allMethods[i].substr("cmd_".length));
      }
    }
    this.getHelpFileText(cmdMethods, function(text) {
      msg.reply(text);
    });
  }

  cmd_list_admins(msg) {
    msg.reply("Liste meiner Admins: "+(_uh.adminConfig.admins.join(", ")));
  }


  cmd_change_prefix(msg, args) {
    var prefix = args[0];
    if(prefix) {
      _system.setCmdPrefix(prefix);
      _system.writeConfig();
      msg.reply("Mein neuer Prefix ist "+prefix);
    }
  }

  cmd_change_avatar(msg, args) {
    var url = args[0];
    if(url) {
      _system.setAvatar(url, function(err) {
        if(err) {
          console.error("Fehler beim Avatar setzen:"+err );
        } else {
          msg.reply("Ich habe ein neues Avatar");
        }
      });
    }
  }

  cmd_bot_ip(msg) {
    getIP((err, ip) => {
      if(!err) {
        msg.reply("Meine IP lautet "+ip);
      }
    });
  }

  cmd_get_services(msg) {
    this.getServicesText(_serviceCommands.services, function(text) {
      msg.reply(text);
    });
  }

  cmd_add_service(msg, clsname) {
    _serviceCommands.register(clsname[0]);
  }

  cmd_remove_service(msg, clsname) {
    _serviceCommands.remove(clsname[0]);
  }

  cmd_update_bot(msg, args) {

    msg.reply("Wirklich Bot updaten? Dies führt git pull, npm update und anschließend den Neustart des Bots aus? Bitte antworte mit j/n");
    _helper.waitForYesNo(msg,() => {
      var messageToShow = args.join(" ");
      var shellcmd = "%cwd%/bin/restartBot.sh";
      msg.channel.sendMessage(messageToShow);
      if(messageToShow) {
        messageToShow = " : "+messageToShow;
      }
      msg.reply("Bot wird geupdated"+messageToShow);
      this.exec_shell(msg, shellcmd);
    });
  }

  cmd_execute_shell(msg, args) {
    var shellcmd = args.join(" ");
    msg.reply("Bist du dir sicher, dass ich diesen Befehl ausführen soll (Dann antworte mit j)?: "+shellcmd);
    _helper.waitForYesNo(msg,() => {
      this.exec_shell(msg, shellcmd);
    });
  }
  // CMD Section End


  exec_shell(msg, shellcmd) {
    var shellcmd = args.join(" ");
    shellcmd = shellcmd.replace(/%cwd%/g,cwd);

    var output = shell.exec(shellcmd).stdout;
    msg.reply(output);
    console.log(output);
  }

  getHelpFileText(data, cb) {
    var I = this;
    if(this.helpText) {
      cb(this.helpText);
      return false;
    }
    _helper.renderHandleBars(null,__dirname+"/cmd_help.hb", {"cmds" : data}, function(text) {
      I.helpText = text;
      cb(text)
    });
  }

  getServicesText(data, cb) {
    var I = this;
    if(this.servicesText) {
      cb(this.servicesText);
      return false;
    }
    _helper.renderHandleBars(null,__dirname+"/cmd_services.hb", {"services" : data}, function(text) {
      this.servicesText = text;
      cb(text);

    });
  }









};