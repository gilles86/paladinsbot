/**
 * Created by Gilless on 23.02.2017.
 */
const fs = require("fs");
module.exports = class Stats_Bot {
  constructor() {
    this._stats = this.loadStats()||{};
    this.intervalSave();
    this.listenToEvents();

  }


  execute(msg, args) {

    var user = null;

    if(args[0]) {
      user = _uh.getMentionedUser(msg.mentions)
    }

    var data = this._stats[(user) ? user.id : msg.author.id];

    this.getStatsText(data,function(text) {
      msg.reply(text);
    })
  }

  getStatsText(data,cb) {
    var I = this;
    _helper.renderHandleBars(null,__dirname+"/statsTemplate.hb", {"stats" : data}, function(text) {
      cb(text);
    });
  }

  listenToEvents() {
    events.on("command", (opt) => {
      if(opt.msg) {
        this.addUser(opt.msg.author.id);
        this.addCmd(opt.msg.author.id, opt.cmdname);
        this.countUpCmd(opt.msg.author.id, opt.cmdname);
      } else {
        console.log("No Message");
      }
    });

    events.on("message", (opts) => {
      if(opts.msg) {
        this.addUser(opts.msg.author.id);
        this.addCmd(opts.msg.author.id, "msg");
        this.countUpCmd(opts.msg.author.id, "msg", opts.msg.content.trim().length);
      } else {
        console.log("No Message");
      }
    });
  }


  countUpCmd(username, cmdName , counter) {
    counter = counter || 1;
    this._stats[username][cmdName] = this._stats[username][cmdName] + counter;
  }

  addCmd(username, cmdName) {
    if(this._stats[username] && !this._stats[username][cmdName]) {
      this._stats[username][cmdName] = 0;
    }
  }

  addUser(username) {
    if(!this._stats[username]) {
      this._stats[username] = {};
    }
  }


  loadStats() {
    return JSON.parse(fs.readFileSync(_fm.getDataFolder()+"/stats.json"));
  }

  writeStats() {
    var statsPath = _fm.getDataFolder()+"/stats.json";
    fs.writeFileSync(statsPath,JSON.stringify(this._stats),"utf-8")
  }

  intervalSave() {
    if(!this.saveInterval) {
      setInterval(() => {
          this.writeStats();
      }, 1000 * 40); // Alle 40sek abspeichern
    }
  }

};