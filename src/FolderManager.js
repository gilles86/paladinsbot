/**
 * Created by Gilles on 05.02.2017.
 */
module.exports = class FolderManager{

  constructor(cfg) {
    this.cfg = cfg;
  }

  getDataFolder() {
    return cwd+"/"+this.cfg.datafolder;
  }

}