/**
 * Created by Gilles on 05.02.2017.
 */
const fs = require("fs");
const base64Img = require("base64-img");

module.exports = class System {

  constructor() {
    this.readConfig();
  }

  readConfig() {
    this.cfg = JSON.parse(fs.readFileSync(cwd+'/config.json', 'utf8'));
  }

  writeConfig() {
    fs.writeFileSync(cwd+'/config.json',JSON.stringify(this.cfg),"utf-8");
  }

  getConfig() {
    return this.cfg;
  }

  setCmdPrefix(prefix) {
    this.cfg.cmdprefix = prefix;
    _commandHandler.prefix = prefix;
  }

  setAvatar(url,cb) {
    base64Img.requestBase64(url, function(err, res, body) {
      if(err) {
        cb(err)
      } else {
        _client.user.setAvatar(body,cb);
      }
    });
  }
};