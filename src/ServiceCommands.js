/**
 * Created by Gilless on 23.02.2017.
 *
 * ServiceCommands sind Commands, die beim connecten des Bots registriert werden müssen
 */

const fs = require("fs");

module.exports = class ServiceCommands {


  constructor(commandHandler, opt) {
    if(typeof opt != "object") {
      opt = {};
    }

    this._commandHandler = commandHandler;
    this.cmdfolder = opt.cmdfolder||"commands";
    this.fixOptions();

    this._services = this.loadServices().services||[];

  }

  fixOptions() {
    if(!this.cmdfolder.endsWith("/")) {
      this.cmdfolder += "/";
    }
  }


  start() {
    for(var i=0; i < this._services.length; i++) {
      this.loadService(this._services[i]);
    }
  }

  loadService(servicename) {
    if(!this._commandHandler.commands[servicename]) {
      var cmdClass = require(cwd+"/src/"+this.cmdfolder+servicename+"/"+servicename+".js");
      this._commandHandler.commands[servicename] = new cmdClass();
    }
  }

  loadServices() {
    return JSON.parse(fs.readFileSync(_fm.getDataFolder()+"/services.json"));
  }

  writeServices() {
    var servicesPath = _fm.getDataFolder()+"/services.json";
    fs.writeFileSync(servicesPath,JSON.stringify({"services" :this._services}),"utf-8")
  }

  register(clsname) {
    this._services.push(clsname);
    this.writeServices();
    this.loadService(clsname);
  }

  remove(clsname) {
    let index = this._services.indexOf(clsname);
    this._services.splice(index, 1);
    this.writeServices();
  }


  get services() {
    return this._services;
  }
};
