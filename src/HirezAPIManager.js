/**
 * Created by Gilles on 14.04.2017.
 */
const Paladins = require('paladins-api');

class HirezAPIManager {
  
  constructor(devid, authkey, sessionId=null) {
    this.renewSessionAfterNoCmd = 15 * 60 * 1000; // 20 Min
    this.renewSessionForce = 15 * 60 * 1000; // 6 Hours
    
    this.api = new Paladins(devid, authkey);
    this.session = sessionId;
    this.sessionCreated = sessionId ? (new Date()).getTime() : null;
    this.lastExecution = (new Date()).getTime();
    
    if(!this.session) {
      this.renewSession(function() {
        console.log("Hirez Session started");
      });
    }
  }
  
  
  cmd_player(name, cb) {
    this.api.getPlayer(this.session, "PC", name, cb);
  }
  
  cmd_ranks(name, cb) {
    this.api.getChampionRanks(this.session,"PC", name, cb);
  }
  
  cmd_history(name, cb) {
    this.api.getMatchHistory(this.session,"PC", name, cb);
  }
  
  cmd_lastmatch(name, cb) {
    this.cmd_history(name, (err, data) => {
      if(!err) {
        let matchId = data[0].matchId;
        this.api.getMatchDetails(session, "PC",matchId, cb);
      } else {
        cb(err);
      }
    });
  }
  
  
  
  renewSession(cb) {
    this.api.connect("PC",(err, sessionid) => {
      if(!err) {
        this.session = sessionid;
        this.sessionCreated = (new Date()).getTime();
      } else {
       console.log("Error while connecting to Hirez API: ", err);
      }
      cb();
    });
  }
  
  execute(command, args, cb) {
    if(!this["cmd_"+command]) {
      cb(null);
      return false;
    }
    if(typeof args == "string") {
      args = [args];
    }
    let now = (new Date()).getTime();
    if(this.needsSessionRecreation()) {
      this.renewSession(()=> {
        this["cmd_"+command](...args, cb);
        this.lastExecution = now;
      });
    } else {
      this["cmd_"+command](...args, cb);
      this.lastExecution = now;
    }
  }
  
  needsSessionRecreation() {
    let now = (new Date()).getTime();
    let lastExec = (now - this.lastExecution) > this.renewSessionAfterNoCmd;
    let forceSession = (now - this.sessionCreated) > this.renewSessionForce;
    return lastExec||forceSession||!this.session;
  }
  
  
  
}

module.exports = HirezAPIManager;