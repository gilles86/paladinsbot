/**
 * Created by Gilles on 02.02.2017.
 */
const fs = require("fs");

/**
 * @typedef {(Discord.Message)} Message
 */
module.exports = class CommandHandler {

  constructor(opt, cmdPrefixes) {
    this.lastCommand = 0;
    if(typeof opt != "object") {
      opt = {};
    }

    this.cmdPrefixes = cmdPrefixes||{};

    this.prefix = opt.cmdprefix||"-";
    this.cmdfolder = opt.cmdfolder||"commands";



    this.commands = {};
    this.fixOptions();
  }

  fixOptions() {
    if(!this.cmdfolder.endsWith("/")) {
      this.cmdfolder += "/";
    }
  }

  /**
   * @param {Message} msg
   */
  isCommand(msg) {
    var contentString = msg.content;
    return (contentString.startsWith(this.prefix));
  }

  getSpecialPrefix(commandName) {
    for(var key in this.cmdPrefixes) {
      var currentCmdName = this.cmdPrefixes[key];
      if(currentCmdName == commandName) {
        return key;
      }
    }
    return "";
  }

  isSpecialCommand(msg) {
    var contentString = msg.content;
    var prefixes = Object.keys(this.cmdPrefixes);
    for(var i=0; i < prefixes.length; i++ ) {
     if(contentString.startsWith(prefixes[i])) {
       return true;
     }
    }
    return false;
  }

  getSpecialCommandName(msg) {
    if(!this.isSpecialCommand(msg)) {
      return COMMAND.NO_COMMAND;
    }

    var contentString = msg.content;
    var prefixes = Object.keys(this.cmdPrefixes);
    for(var i=0; i < prefixes.length; i++ ) {
      if(contentString.startsWith(prefixes[i])) {
        return this.cmdPrefixes[prefixes[i]];
      }
    }
    return COMMAND.NO_COMMAND;
  }

  /**
   * @param {Message} msg
   */
  getCommandName(msg) {
    if(typeof msg.content !="string" && this.isCommand(msg)) {
      return COMMAND.NO_COMMAND;
    }

    var commandString = msg.content.substr(this.prefix.length);
    if(commandString.length > 1) {
      commandString = commandString.split(" ")[0];
      commandString = commandString.replace(/\//g,"");
      commandString = commandString.replace(/\./g,"");
      return commandString;
    }
    return COMMAND.WRONG_FORMAT;
  }

  /**
   * @param {Message} msg
   */
  execute(msg) {
    var commandName = this.getCommandName(msg);
    var specialCommandName = this.getSpecialCommandName(msg);
    if(commandName == COMMAND.NO_COMMAND && specialCommandName == COMMAND.NO_COMMAND) {
      return false;
    }
    if(commandName == COMMAND.WRONG_FORMAT) {
      return commandName;
    }

    if(this.isSpecialCommand(msg)) {
      var cmd = this.getCommandObject(specialCommandName);
      return this.executeCmd(cmd, msg, true)
    } else {
      var cmd = this.getCommandObject(commandName);
      return this.executeCmd(cmd, msg);
    }

  }


  executeCmd(cmd, msg, isSpecial) {
    if(cmd == COMMAND.NOT_FOUND) {
      return cmd;
    }

    if(cmd && cmd.execute) {

      var args = null;
      if(isSpecial) {
         args = this.getSpecialCommandArguments(msg);
      } else {
         args =this.getCommandArguments(msg)
      }
      if(!this.inRecursion(msg,cmd)) {
        cmd.execute(msg, args);
        let cmdName = this.getCommandName(msg);
        events.trigger("command", {
          "cmdname" : cmdName
        , "msg" : msg, "args": args});
      } else {
        msg.reply("Rekursionen sind blöd.. hör auf damit ");
      }

    } else {
      console.error("DIE KLASSE FUER COMMAND "+commandName+" HAT KEINE EXECUTE METHODE");
    }
  }

  inRecursion(msg,cmd) {
    if(msg.author == _client.user) {
      this.lastCommand++;
      if(this.lastCommand >= 3) {
        this.lastCommand = 0;
        return true;
      }
    } else {
      this.lastCommand = 0;
    }
    return false;
  }

  getCommandArguments(msg) {
    var args = null

    if(this.isValidCommand(msg)) {
      var cmdName = this.getCommandName(msg);
      var firstCmdPart = this.prefix+cmdName+" ";
      args = msg.content.substr(firstCmdPart.length).trim();
    }
    return args;
  }

  getSpecialCommandArguments(msg) {
    var args = null

    var cmdName = this.getSpecialCommandName(msg);
    var firstCmdPart = this.getSpecialPrefix(cmdName);
    args = msg.content.substr(firstCmdPart.length).trim();

    return args;
  }

  isValidCommand(msg)  {
    if(this.isCommand(msg)) {
      var cmdName = this.getCommandName(msg);
      return cmdName != COMMAND.NOT_FOUND && cmdName != COMMAND.WRONG_FORMAT;
    }
    return false;
  }

  getCommandObject(commandName) {
    var cmdObject = null;
    if(this.commands[commandName]) {
      cmdObject = this.commands[commandName];
    } else {
      try {
        var cmdClass = require("./"+this.cmdfolder+commandName+"/"+commandName+".js");
        this.commands[commandName] = new cmdClass();
        cmdObject = this.commands[commandName];
      } catch (e) {
        console.error(e);
        cmdObject = COMMAND.NOT_FOUND;
      }
    }
    return cmdObject;
  }
};