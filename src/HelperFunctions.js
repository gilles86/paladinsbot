/**
 * Created by Gilles on 05.02.2017.
 */
const fs = require("fs");
const moment = require("moment");

module.exports = {
  "getAllClassMethods" : function(obj) {
    let methods = [];
    while (obj = Reflect.getPrototypeOf(obj)) {
      let keys = Reflect.ownKeys(obj)
      keys.forEach((k) => methods.push(k));
    }
    return methods;
  },
  "renderHandleBars" : function(msg, pathname, data, cb) {
    if(!this.templates) {
      this.templates = {};
    }

    if(this.templates[pathname]) {
      var template = _hb.compile(file);

      if(!data) {
        data = {};
      }
      data.cfg  = _cfg;
      var msgString = template(data);
      if(msg) {
        msg.reply(msgString);
      }
      if(typeof cb == "function") {
        cb(msgString);
      }
      return false;
    }

    fs.readFile(pathname,"utf-8", function(err, file) {
      if(err) {
        console.error("Fehler beim Rendern des Handlebar Templates : "+err);
        return false;
      }
      var template = _hb.compile(file);

      if(!data) {
        data = {};
      }
      data.cfg  = _cfg;
      var msgString = template(data);
      if(msg) {
        msg.reply(msgString);
      }
      if(typeof cb == "function") {
        cb(msgString);
      }
    });
  },
  waitForYesNo: function(msg,args,cb) {

    if(args&& !cb) {
      cb = args;
      args = { max: 3, time: 20000, errors: ['time'] };
    }

    var allowed = ["yes","y","ja","j","no","n","nein"];
    msg.channel.awaitMessages(function(msg, collector) {
      canStop = (allowed.indexOf(msg.content.trim().toLowerCase()) >= 0);
      if(canStop) {
        collector.stop();
      }
      return canStop;
    },args).then(newmsgs => {
      var lastcontent = newmsgs.last().content.trim().toLowerCase();
      if(lastcontent.startsWith("j") || lastcontent.startsWith("y")) {
        cb(msg);
      }
    });
  },
  durationInHours(utcDateString) {
    let day = moment(utcDateString);
    return moment().utc().diff(day.utc(),"hours");
  },
  durationInDays(utcDateString) {
    let day = moment(utcDateString);
    return moment().utc().diff(day.utc(),"days");
  }
};