/**
 * Created by Gilles on 05.02.2017.
 */
const fs = require("fs");
module.exports = class UserHandler {

  constructor() {
    this.adminConfig = this.readAdminsConfig();
  }



  removeAdminUser(user) {
    var fullname = this.getUserNameFromUser(user);
    var index = this.adminConfig.admins.indexOf(fullname);
    if(index >= 0) {
      this.adminConfig.admins.splice(index,1);
      _uh.writeAdminsConfig();
    }
  }

  isAdminUser(user) {
    var fullname = this.getUserNameFromUser(user);
    return this.adminConfig.admins.indexOf(fullname) > -1;
  }

  getUserNameFromUser(user) {
    if(!user) return "";
    return user.username+"#"+user.discriminator;
  }

  getMentionedUser(mentions) {
    if(mentions.users.first()) {
      return mentions.users.first();
    } else {
      return null;
    }
  }

  addAdminUser(user) {
    var fullname = this.getUserNameFromUser(user);
    this.adminConfig.admins.push(fullname);
    _uh.writeAdminsConfig();
  }



  readAdminsConfig() {
    var adminConfigPath = _fm.getDataFolder()+"/admins.json";
    try {
      var file = fs.readFileSync(adminConfigPath, "utf-8");
      var data = JSON.parse(file);
      return data;
    } catch(e) {
      console.error("Fehler beim lesen der Admins Config "+e);
      return {
        "admins" : []
      };
    }

  }

  writeAdminsConfig() {
    var adminConfigPath = _fm.getDataFolder()+"/admins.json";
    fs.writeFileSync(adminConfigPath,JSON.stringify(this.adminConfig),"utf-8")
  }

};